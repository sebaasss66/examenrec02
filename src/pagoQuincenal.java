/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sebas
 */
public class pagoQuincenal {
    
    private int numRecibo;
    private String nombre;
    private int puesto;
    private int nivel;
    private int diasTrabajados;


    public pagoQuincenal() {
        this.numRecibo=0;
        this.nombre="";
        this.puesto=0;
        this.puesto=0;
        this.nivel=0;
        this.diasTrabajados=0;
    }

    public pagoQuincenal(int numRecibo, String nombre, int puesto, int nivel, int diasTrabajados) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.puesto = puesto;
        this.nivel = nivel;
        this.diasTrabajados = diasTrabajados;
    }
    
    public pagoQuincenal(pagoQuincenal otro){
        this.numRecibo = otro.numRecibo;
        this.nombre = otro.nombre;
        this.puesto = otro.puesto;
        this.nivel = otro.nivel;
        this.diasTrabajados = otro.diasTrabajados;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    
    float calcularPago(){
        final float puesto1_Auxiliar = 100;
        final float puesto2_Albanil = 200;
        final float puesto3_Ingobra = 300;
        float pago = 0;
        switch (puesto) {
            case 1:
                pago = puesto1_Auxiliar * diasTrabajados;
                break;
            case 2:
                pago = puesto2_Albanil * diasTrabajados;
                break;
            case 3:
                pago = puesto3_Ingobra * diasTrabajados;
                break;
            default:
                System.out.println("Puesto inválido.");
        }
        return pago;

    }
    
    float calcularImpuesto(){
        final float NIVEL1_IMPUESTO = 0.05f; // 5%
        final float NIVEL2_IMPUESTO = 0.03f; // 3%

        float impuesto = 0;
        if (nivel == 1) {
            impuesto = calcularPago() * NIVEL1_IMPUESTO;
        } else if (nivel == 2) {
            impuesto = calcularPago() * NIVEL2_IMPUESTO;
        } else {
            System.out.println("Nivel inválido.");
        }
        return impuesto;

    }
    
    float calcularTotal(){
        return calcularPago() - calcularImpuesto();

    }
    
    
}
